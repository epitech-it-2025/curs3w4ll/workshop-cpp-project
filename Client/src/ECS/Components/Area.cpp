#include "ECS/Components/Area.hpp"

ecs::component::Area::Area(sf::Vector2f size, bool centered) noexcept :
    size(size), centered(centered)
{
}

ecs::component::Area& ecs::component::Area::setSize(sf::Vector2f size) noexcept
{
    this->size = size;
    return *this;
}

ecs::component::Area& ecs::component::Area::setCentered(bool centered) noexcept
{
    this->centered = centered;
    return *this;
}

sf::Vector2f ecs::component::Area::getSize() const noexcept
{
    return this->size;
}

bool ecs::component::Area::getCentered() const noexcept
{
    return this->centered;
}
