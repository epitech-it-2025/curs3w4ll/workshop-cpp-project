#include "Game/Parallax.hpp"

#include "ECS/Components/Attributes.hpp"
#include "ECS/Components/Parallax.hpp"
#include "ECS/Components/RenderableImage.hpp"
#include "ECS/Components/Transform.hpp"
#include "ECS/Core/Coordinator.hpp"

game::ParallaxBackground::ParallaxBackground(ecs::core::Coordinator& coordinator, const std::string& filepath, unsigned int speed) :
    coordinator(coordinator), background(coordinator.createEntity()), speed(speed)
{
    coordinator.setComponent<ecs::component::RenderableImage>(this->background, filepath, sf::Vector2i{-1, -1}, false);
    coordinator.setComponent<ecs::component::Attributes>(this->background);
    coordinator.setComponent<ecs::component::Transform>(this->background, {.movement = {-this->speed, 0}});
    coordinator.setComponent<ecs::component::Parallax>(this->background);
}

game::ParallaxBackground::ParallaxBackground(ParallaxBackground&& other) noexcept :
    coordinator(other.coordinator), background(std::move(other.background)), speed(other.speed), valid(other.valid)
{
    other.valid = false;
}

game::ParallaxBackground::~ParallaxBackground() noexcept
{
    if (this->valid) {
        this->coordinator.destroyEntity(this->background);
    }
}

game::ParallaxManager::ParallaxManager(ecs::core::Coordinator& coordinator, const std::vector<std::string>& filepaths, unsigned int minSpeed, unsigned int maxSpeed) noexcept :
    minSpeed(minSpeed), maxSpeed(maxSpeed)
{
    unsigned int nbFilepaths = filepaths.size();
    this->backgrounds.reserve(nbFilepaths);

    unsigned int speed = minSpeed;
    unsigned int speedStep = (maxSpeed - minSpeed) / (nbFilepaths - 1);

    for (auto const& filepath : filepaths) {
        this->backgrounds.emplace_back(coordinator, filepath, speed);
        speed += speedStep;
    }
}