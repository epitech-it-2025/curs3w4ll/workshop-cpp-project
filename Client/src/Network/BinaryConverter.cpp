#include "Network/BinaryConverter.hpp"

#include "SFML/System/Vector2.hpp"

#include <iostream>
#include <string>
#include <vector>

std::vector<Byte> BinaryConverter::convertVector2f(sf::Vector2f& pos) noexcept
{
    ecs::core::Bitset bitset;

    int i = 63;

    int const x = static_cast<int>(pos.x);
    int const y = static_cast<int>(pos.y);

    std::vector<Byte> xByte = this->convertIntToBinary(x);
    std::vector<Byte> yByte = this->convertIntToBinary(y);

    for (const auto& byte : xByte) {
        if (byte == 1) {
            bitset.set(i, true);
        } else {
            bitset.set(i, false);
        }
        i--;
    }

    for (const auto& byte : yByte) {
        if (byte == 1) {
            bitset.set(i, true);
        } else {
            bitset.set(i, false);
        }
        i--;
    }
    std::vector<Byte> result = bitset.extractData();
    return result;
}

sf::Vector2f BinaryConverter::convertBinaryToVector2f(std::vector<Byte>& data) noexcept
{

    sf::Vector2f pos;

    // NOLINTNEXTLINE
    pos.y = *reinterpret_cast<int*>(data.data());
    data.erase(data.begin(), data.begin() + sizeof(int));
    // NOLINTNEXTLINE
    pos.x = *reinterpret_cast<int*>(data.data());
    data.erase(data.begin(), data.begin() + sizeof(int));

    return pos;
}

std::vector<Byte> BinaryConverter::convertIntToBinary(int data)
{
    std::vector<Byte> binaryData;

    while (data > 0) {
        binaryData.push_back(data % 2);
        data = data / 2;
    }
    std::reverse(binaryData.begin(), binaryData.end());
    while (binaryData.size() != 32) {
        binaryData.insert(binaryData.begin(), 0);
    }

    return binaryData;
}

std::vector<Byte> BinaryConverter::convertUShortToBinary(unsigned short offset)
{
    std::vector<Byte> convertedValue;
    std::vector<Byte> binaryData;
    int i = 15;
    ecs::core::Bitset bitset;

    while (offset > 0) {
        binaryData.push_back(offset % 2);
        offset = offset / 2;
    }
    std::reverse(binaryData.begin(), binaryData.end());
    while (binaryData.size() != 16) {
        binaryData.insert(binaryData.begin(), 0);
    }

    for (const auto& byte : binaryData) {

        if (byte == 1) {
            bitset.set(i, true);
        } else {
            bitset.set(i, false);
        }
        i--;
    }
    return bitset.extractData();
}

unsigned short BinaryConverter::convertBinaryToUShort(std::vector<Byte>& rowData) noexcept
{
    // NOLINTNEXTLINE
    unsigned short offCode = *reinterpret_cast<unsigned short*>(rowData.data());
    rowData.erase(rowData.begin(), rowData.begin() + sizeof(unsigned short));
    return offCode;
}
