/**
 * @file UpdateDataFromNetwork.hpp
 * @brief UpdateDataFromNetwork file
 * @author Baptiste-MV
 * @version 1
 */

#pragma once

#include "ECS/Components/Attributes.hpp"
#include "ECS/Components/RenderableImage.hpp"
#include "ECS/Core/Coordinator.hpp"
#include "Game/Bullet.hpp"
#include "Game/Enemy.hpp"
#include "Network/ClientNetwork.hpp"

namespace game {
/**
 * @class UpdateDataFromNetwork
 * @brief class to create enemy that cannot shot
 */
class UpdateDataFromNetwork {
   public:
    UpdateDataFromNetwork() noexcept = delete;
    /**
     * @brief Create a manager to update the data received from server
     *
     * @param coordinator The ECS coordinator
     * @param client The client
     */
    UpdateDataFromNetwork(ecs::core::Coordinator& coordinator, network::ClientNetwork& client, game::BulletManager& bulletManager, game::EnemyManager& enemyManager);
    UpdateDataFromNetwork(UpdateDataFromNetwork const& other) noexcept = delete;
    UpdateDataFromNetwork(UpdateDataFromNetwork&& other) noexcept = delete;
    ~UpdateDataFromNetwork() noexcept = default;

    UpdateDataFromNetwork& operator=(UpdateDataFromNetwork const& other) noexcept = delete;
    UpdateDataFromNetwork& operator=(UpdateDataFromNetwork&& other) noexcept = delete;

    /**
     * @brief Update
     */
    void update();

    void setPlayerEntityId(const size_t entityId) noexcept;
    void setEnemyEntityId(unsigned short id, const size_t entityId) noexcept;
    void setProjectileEntityId(unsigned short id, const size_t entityId) noexcept;

    short getProjectileId(const size_t& entityId) const noexcept;

    const std::unordered_map<unsigned short, size_t>& getProjectileEntityIds() const noexcept;

   private:
    /**
     * @var scalePlayer
     * @brief The scale of the player sprite
     */
    constexpr static float scalePlayer = 3;
    /**
     * @var areaPlayer
     * @brief The size of the player sprite
     */
    constexpr static sf::Vector2i areaPlayer{32, 14};
    /**
     * @var pathPlayer
     * @brief The filepath to the player sprite
     */
    constexpr static std::string_view pathPlayer = "player/spaceShipUp.png";


    /**
     * @brief The ECS coordinator
     *
     */
    ecs::core::Coordinator& coordinator;
    /**
     * @var client
     * @brief Client socket linked to the server
     */
    network::ClientNetwork& client;

    game::BulletManager& bulletManager;

    game::EnemyManager& enemyManager;

    std::unordered_map<unsigned short, size_t> playerEntityIds;
    std::unordered_map<unsigned short, size_t> enemyEntityIds;
    std::unordered_map<unsigned short, size_t> projectileEntityIds;
};

} // namespace game