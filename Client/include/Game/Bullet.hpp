/**
 * @file Buller.hpp
 * @author Afilak
 * @brief game file for bullet of player and enemies
 * @version 0.1
 */

#pragma once

#include "ECS/Components/Animation.hpp"
#include "ECS/Components/Attributes.hpp"
#include "ECS/Components/Bounds.hpp"
#include "ECS/Components/RenderableImage.hpp"
#include "ECS/Components/Transform.hpp"
#include "Graphic/EventHandler.hpp"
#include "Graphic/Renderer.hpp"
#include "Network/ClientNetwork.hpp"

#include <string>
#include <unordered_map>

namespace game {

/**
 * @enum BulletType
 * @brief The different types of bullets
 */
enum BulletType {
    ELECTRIC = 0,
    BALL = 1,
};

/**
 * @class ABullet
 * @brief The bullet graphic base element
 */
class ABullet {
   public:
    ABullet() noexcept = delete;
    ABullet(ecs::core::Coordinator& coordinator, sf::Vector2f position, sf::Vector2f direction, float speed, float scale);
    ABullet(ABullet const& other) noexcept = delete;
    ABullet(ABullet&& other) noexcept;
    virtual ~ABullet() noexcept;

    ABullet& operator=(ABullet const& other) noexcept = delete;
    ABullet& operator=(ABullet&& other) noexcept = delete;

    /**
     * @brief Check if the bullet is alive
     *
     * @return `true` if the ennemy is alive, `false` otherwise
     */
    bool isAlive() const noexcept;
    /**
     * @brief The function to call when the bullet hit something
     */
    virtual void hit() noexcept;
    /**
     * @brief The function to call when the bullet should be destroyed
     */
    virtual void destroy() noexcept;
    /**
     * @brief Update bullet movement
     *
     * @param elapsedTime The time passed since the last call
     */
    virtual void move(float elapsedTime) noexcept = 0;

    const size_t getEntityId() const noexcept;

   protected:
    /**
     * @var coordinator
     * @brief The ECS coordinator
     */
    ecs::core::Coordinator& coordinator;
    /**
     * @var entity
     * @brief The ECS entity
     */
    ecs::core::Entity entity;
    /**
     * @var valid
     * @brief Set to `true` if the object is valid
     */
    bool valid = true;
    /**
     * @var lifePoints
     * @brief The life points of the bullet
     */
    int lifePoints = 1;
    /**
     * @var id
     * @brief The player id
     */
    unsigned short id;
};

/**
 * @class ElectricBullet
 * @brief The electric style bullet
 */
class ElectricBullet final : public ABullet {
   public:
    ElectricBullet() noexcept = delete;
    ElectricBullet(ecs::core::Coordinator& coordinator, sf::Vector2f position, sf::Vector2f direction);
    ElectricBullet(ElectricBullet const& other) noexcept = delete;
    ElectricBullet(ElectricBullet&& other) noexcept = delete;
    ~ElectricBullet() noexcept final = default;

    ElectricBullet& operator=(ElectricBullet const& other) noexcept = delete;
    ElectricBullet& operator=(ElectricBullet&& other) noexcept = delete;

    void move(float elapsedTime) noexcept final;

   private:
    /**
     * @var speed
     * @brief The bullet speed
     */
    constexpr static int speed = 400;
    /**
     * @var spriteArea
     * @brief The size of the bullet sprite
     */
    constexpr static sf::Vector2i spriteArea{16, 12};
    /**
     * @var spritePath
     * @brief The filepath to the bullet sprite
     */
    constexpr static std::string_view spritePath = "bullets/electric.png";
    /**
     * @var soundPath
     * @brief The filepath to the bullet sound
     */
    constexpr static std::string_view soundPath = "laserShot.wav";
    /**
     * @var animationFramesNumber
     * @brief The number of frames the animation of the bullet contains
     */
    constexpr static unsigned int animationFramesNumber = 3;
    /**
     * @var animationFrameDuration
     * @brief The time between two frame of the animation
     */
    constexpr static unsigned int animationFrameDuration = 20;
    /**
     * @var scale
     * @brief The scale of the bullet sprite
     */
    constexpr static float scale = 3;
};

/**
 * @class BallBullet
 * @brief The ball style bullet
 */
class BallBullet final : public ABullet {
   public:
    BallBullet() noexcept = delete;
    BallBullet(ecs::core::Coordinator& coordinator, sf::Vector2f position, sf::Vector2f direction);
    BallBullet(BallBullet const& other) noexcept = delete;
    BallBullet(BallBullet&& other) noexcept = delete;
    ~BallBullet() noexcept final = default;

    BallBullet& operator=(BallBullet const& other) noexcept = delete;
    BallBullet& operator=(BallBullet&& other) noexcept = delete;

    void move(float elapsedTime) noexcept final;

   private:
    /**
     * @var speed
     * @brief The bullet speed
     */
    constexpr static int speed = 1000;
    /**
     * @var spriteArea
     * @brief The size of the bullet sprite
     */
    constexpr static sf::Vector2i spriteArea{7, 6};
    /**
     * @var spritePath
     * @brief The filepath to the bullet sprite
     */
    constexpr static std::string_view spritePath = "bullets/ball.png";
    /**
     * @var soundPath
     * @brief The filepath to the bullet sound
     */
    constexpr static std::string_view soundPath = "laserShot.wav";
    /**
     * @var animationFramesNumber
     * @brief The number of frames the animation of the bullet contains
     */
    constexpr static unsigned int animationFramesNumber = 4;
    /**
     * @var animationFrameDuration
     * @brief The time between two frame of the animation
     */
    constexpr static unsigned int animationFrameDuration = 30;
    /**
     * @var scale
     * @brief The scale of the bullet sprite
     */
    constexpr static float scale = 3;
};

/**
 * @class BulletManager
 * @brief Create, update and delete bullets
 */
class BulletManager {
   public:
    explicit BulletManager(network::ClientNetwork& client) noexcept;
    BulletManager(BulletManager const& other) noexcept = delete;
    BulletManager(BulletManager&& other) noexcept = delete;
    ~BulletManager() noexcept = default;

    BulletManager& operator=(BulletManager const& other) noexcept = delete;
    BulletManager& operator=(BulletManager&& other) noexcept = delete;

    /**
     * @brief Update the existing bullets
     *
     * @param elapsedTime The time elapsed since the last call
     */
    void updateBullets(float elapsedTime,  const std::unordered_map<unsigned short, size_t>& projectileEntityIds) noexcept;
    /**
     * @brief Create a new bullet element
     *
     * @param coordinator The ECS coordinator
     * @param type The type of the bullet
     * @param position The position of the bullet
     * @param direction The direction of the bullet
     */
    void createBullet(ecs::core::Coordinator& coordinator, BulletType type, sf::Vector2f position, sf::Vector2i direction) noexcept;
    /**
     * @brief Clear all existing bullets
     */
    void clearBullets() noexcept;

    const size_t getLastBullet() const noexcept;

   private:
    /**
     * @var lastBulletId
     * @brief The id of the last creating entity
     */
    int lastBulletId = 0;
    /**
     * @var listBullets
     * @brief The list of existing bullets
     */
    std::unordered_map<int, std::unique_ptr<game::ABullet>> listBullets{};
    /**
     * @var client
     * @brief Client socket linked to the server
     */
    network::ClientNetwork& client;
};

} // namespace game