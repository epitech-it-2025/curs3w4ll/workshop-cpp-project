/**
 * @file Transform.hpp
 * @brief Transform component to set the transformation Entity
 * @author Baptiste-MV
 * @version 1
 */

#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#pragma once

namespace ecs::component {

/**
 * @struct Transform
 * @brief Transform contains different variable for the transformation Entity
 */
struct Transform {
    /**
     * @var movement
     * @brief movement indicate movement for the Entity
     */
    sf::Vector2f movement{0, 0};
    /**
     * @var grow
     * @brief grow indicate grow for the Entity
     */
    sf::Vector2f grow{0, 0};
    /**
     * @var rotation
     * @brief rotation direction speed for the Entity
     */
    float rotation = 0;
};

} // namespace ecs::component