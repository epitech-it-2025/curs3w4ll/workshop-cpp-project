/**
 * @file Component.hpp
 * @brief Components informations
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "Bitset.hpp"

#include <bitset>
#include <cstddef>

namespace ecs::core {

/**
 * @brief The object to use as a signature of components
 */
using ComponentSignature = Bitset;

} // namespace ecs::core
