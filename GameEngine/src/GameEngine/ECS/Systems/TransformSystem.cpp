#include "GameEngine/ECS/Systems/TransformSystem.hpp"

#include "GameEngine/ECS/Components/Attributes.hpp"
#include "GameEngine/ECS/Components/Bounds.hpp"
#include "GameEngine/ECS/Components/Transform.hpp"

ge::ecs::system::TransformSystem::TransformSystem(ecs::core::Coordinator& coordinator) noexcept :
     ge::ecs::core::ASystem(coordinator) { }

void  ge::ecs::system::TransformSystem::update(float elapsedTime)
{
    for (auto const& entity : this->entities) {
        auto& attributes = this->coordinator.getComponent<ecs::component::Attributes>(entity);
        auto& transform = this->coordinator.getComponent<ecs::component::Transform>(entity);

        attributes.angle += (transform.rotation * elapsedTime) / float{1000};
        attributes.scale += (transform.grow * elapsedTime) / float{1000};
        attributes.position += (transform.movement * elapsedTime) / float{1000};
    }
}