/**
 * @file ECSErrors.hpp
 * @brief Errors classes for ECS related errors
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include <exception>
#include <string>

/**
 * @namespace ge::ecs::error
 * @brief Namespace that contain all error classes than can be throw for the ECS
 */
namespace ge::ecs::error {

/**
 * @class ECSError
 * @brief Lower-level class for ECS error
 *
 * All error classes related to the ECS should inherit from this class
 */
class ECSError : public std::exception {
   public:
    ECSError() noexcept = delete;
    /**
     * @brief Generate an object to throw when an error occurs in the ECS
     *
     * @param message A message that describe the error
     */
    explicit ECSError(std::string const& message);
    ECSError(ECSError const& other) = default;
    ECSError(ECSError&& other) = default;
    ~ECSError() override = default;

    ECSError& operator=(ECSError const& other) = default;
    ECSError& operator=(ECSError&& other) = default;

    /**
     * @brief Get a description of the error
     *
     * @return The error message defined in constructor
     */
    const char* what() const noexcept override;

   protected:
    /**
     * @brief Store the message gived in the constructor that describe the error
     */
    std::string message;
};

class ComponentError : public ECSError {
   public:
    ComponentError() noexcept = delete;
    /**
     * @brief Generate an object to throw when an error occurs in an ECS component
     *
     * @param name The name of the component where an error occured
     * @param message The error message that describe the error
     */
    explicit ComponentError(std::string const& name, std::string const& message);
    ComponentError(ComponentError const& other) = default;
    ComponentError(ComponentError&& other) = default;
    ~ComponentError() override = default;

    ComponentError& operator=(ComponentError const& other) = default;
    ComponentError& operator=(ComponentError&& other) = default;

    /**
     * @brief Get the name of the component realted to the error
     *
     * @return The component name
     */
    const char* which() const noexcept;

   protected:
    /**
     * @brief The name of the component which the error is from
     */
    std::string name;
};

} // namespace ge::ecs::error
