/**
 * @file Component.hpp
 * @brief Components informations
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "GameEngine/Utils/Bitset.hpp"

#include <bitset>
#include <cstddef>

namespace ge::ecs::core {

/**
 * @brief The object to use as a signature of components
 */
using ComponentSignature = utils::Bitset;

} // namespace ge::ecs::core
