/**
 * @file Area.hpp
 * @brief An invisible area object
 * @author curs3w4ll
 * @version 1
 */

#pragma once

#include "GameEngine/Utils/Vector.hpp"
#include "SFML/System/Vector2.hpp"

namespace ge::ecs::component {

/**
 * @class Area
 * @brief Invisible graphic area
 */
class Area {
   public:
    Area() noexcept = delete;
    /**
     * @brief Construct a new Area object
     * 
     * @param size size of the area
     * @param centered if the area is centered
     */
    explicit Area(utils::Vector size, bool centered = true) noexcept;
    Area(Area const& other) noexcept = default;
    Area(Area&& other) noexcept = default;
    ~Area() noexcept = default;

    Area& operator=(Area const& other) noexcept = default;
    Area& operator=(Area&& other) noexcept = default;

    /**
     * @brief Set the size of the area
     *
     * @param size The size, width and height
     *
     * @return The area itself
     */
    Area& setSize(utils::Vector size) noexcept;
    /**
     * @brief Set if the area position should be centered from the size
     *
     * @param centered Set to `true` if need to center position
     *
     * @return The area itself
     */
    Area& setCentered(bool centered) noexcept;
    /**
     * @brief Get the size of the area
     *
     * @return The size of the area, width and height
     */
    utils::Vector getSize() const noexcept;
    /**
     * @brief Check if the area position should be centered
     *
     * @return `true` if the area position should be centered, `false` otherwise
     */
    bool getCentered() const noexcept;

   private:
    /**
     * @var size
     * @brief The size of the area
     */
    utils::Vector size;
    /**
     * @var centered
     * @brief Set to `true` if the area position should be centered in the midle of its size
     */
    bool centered;
};

} // namespace ge::ecs::component
