/**
 * @file RenderableText.hpp
 * @brief  RenderableText component to set a sprite on Entity
 * @author Baptiste-MV
 * @version 1
 */

#include "GameEngine/Utils/Vector.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <string_view>

#pragma once

namespace ge::ecs::component {

/**
 * @class RenderableText
 * @brief Create and store sprite and texture
 */
class RenderableText {
   public:
    RenderableText() noexcept = delete;
    /**
     * @brief Construct a new Renderable Text
     *
     * @param content The content of the text
     * @param size The font size of the text
     * @param font The text font
     */
    explicit RenderableText(std::string_view content, bool centered = true, size_t size = 14, std::string_view fontPath = RenderableText::defaultFont);
    RenderableText(RenderableText const& other) noexcept = delete;
    RenderableText(RenderableText&& other) noexcept;
    ~RenderableText() noexcept = default;

    RenderableText& operator=(RenderableText const& other) noexcept = delete;
    RenderableText& operator=(RenderableText&& other) noexcept = default;

    /**
     * @brief Set the content of the text
     *
     * @param content The content to set the text
     *
     * @return A reference to the object itself
     */
    RenderableText& setContent(std::string_view content, bool centered = true) noexcept;
    /**
     * @brief Set the size of the text
     *
     * @param size The size to set the text
     *
     * @return A reference to the object itself
     */
    RenderableText& setSize(size_t size) noexcept;
    /**
     * @brief Set the text font
     *
     * @param fontPath The path to the font file
     *
     * @return A reference to the object itself
     *
     * @throw error::ComponentError if the filepath is invalid
     */
    RenderableText& setFont(std::string_view fontPath);
    /**
     * @brief Set the color of the text
     *
     * @param color The color to set the text
     *
     * @return A reference to the object itself
     */
    RenderableText& setColor(sf::Color color) noexcept;
    /**
     * @brief Set the text position
     *
     * @param position The position to set the text
     *
     * @return A reference to the object itself
     */
    RenderableText& setPosition(utils::Vector position) noexcept;
    /**
     * @brief Set the text scale
     *
     * @param scale The scale to set the text
     *
     * @return A reference to the object itself
     */
    RenderableText& setScale(utils::Vector scale) noexcept;
    /**
     * @brief Set the text rotation
     *
     * @param rotation The rotation to set the text
     *
     * @return A reference to the object itself
     */
    RenderableText& setRotation(float angle) noexcept;
    /**
     * @brief Draw the text on window
     *
     * @param window The window to draw the text
     *
     * @return A reference to the object itself
     */
    RenderableText& draw(sf::RenderWindow& window) noexcept;
    /**
     * @brief Center the text
     *
     * @param centered Set to `true` if the text origin should be centered, `false` otherwise
     *
     * @return A reference to the object itself
     */
    RenderableText& setCentered(bool centered = true) noexcept;
    /**
     * @brief Get the bounds of the text
     *
     * @return The text bounds
     */
    sf::FloatRect getBounds() const noexcept;
    /**
     * @brief Get the color of the text
     *
     * @return The text color
     */
    sf::Color getColor() const noexcept;
    /**
     * @brief Hide the text, do not graphically display it anymore
     *
     * @param hide Set to `true` if need to hide, `false` otherwise
     */
    void hide(bool hide = true) noexcept;
    /**
     * @brief Check if the text should be hidden
     *
     * @return `true` if the text should be hidden and then not displayed
     */
    bool isHidden() const noexcept;

   private:
    /**
     * @var defaultFont
     * @brief The filepath of the default font
     */
    constexpr static std::string_view defaultFont = "score.ttf";
    /**
     * @var hidden
     * @brief Set if the text should be hidden or not
     */
    bool hidden = false;
    /**
     * @var text
     * @brief Text of the Entity
     */
    sf::Text text;
};

} // namespace ge::ecs::component
