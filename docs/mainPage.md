# RType

## Welcome on the code documentation

Welcome on the documentation of the real code. This documentation is created from the code itself.  
People that are writing code, have written documentation about it, and here it is.

You can find all about files, classes, namespaces and much more here.

Feel free to read it !

## Writing documentation

If you want to know how to write some documentation about your code, please refer to [this documentation](https://gitlab.com/epitech-it-2025/curs3w4ll/RType/-/blob/main/doc/doxygen.md).

You should write documentation for every code you write. 

## The project

The RType project is a project from [Epitech](https://www.epitech.eu/) in third year.

The goal of this project is to create a game based on the [R-Type game](https://fr.wikipedia.org/wiki/R-Type) in [c++](https://fr.wikipedia.org/wiki/C%2B%2B).

Your are in the code documentation, written by developpers for developpers, to understand how the code works.

If you want to know more about the project, go check the [Gitlab](https://gitlab.com/epitech-it-2025/curs3w4ll/RType).
