/**
 * @file Offset.hpp
 * @brief The default value list of all the OPCODE
 * @author Benjamin
 * @version 1.0
 */

#pragma once
/**
 * @var PLAYER
 * @brief The opcode value for playerInfo data
 */
static constexpr unsigned short const PLAYER = 1;
/**
 * @var ENEMY
 * @brief The opcode value for enemyInfo data
 */
static constexpr unsigned short const ENEMY = 2;
/**
 * @var PROJECTILE
 * @brief The opcode value for projectile data
 */
static constexpr unsigned short const PROJECTILE = 3;
